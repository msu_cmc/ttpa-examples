#include <stdio.h>
#include "ifs.h"
#include "ifs_types.h"
#include "ifs_rodl.h"
#include "ttpa_task.h"

#include "appl.h"

IFS_RODLFILE(0x00, 7, ifs_int_eep)
{
	IFS_RE_SEND(0x01, IFS_ADDR_I(IO_FN,0x01,0x00), 0),
	IFS_RE_RECV(0x03, IFS_ADDR_I(IO_FN,0x01,0x01), 2),
	IFS_RE_RECV(0x04, IFS_ADDR_I(IO_FN,0x01,0x02), 0),
	IFS_RE_RECV(0x05, IFS_ADDR_I(IO_FN,0x01,0x03), 0),
	IFS_RE_EXEC(0x06, IFS_ADDR_I(APPL_FN,0x01,0x00)),
	IFS_RE_EOR(0x07)
};
