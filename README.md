# README #

Usage examples for TTP/A protocol implementation for 8-bit AVR MCU with hardware UART support.

## SUPPORTED MCUs ##

Master: atmega1280, atmega2560 MCUs.
Slave: atmega328, atmega128, atmega1280, atmega2560 MCUs.

See configuration files `ttpa/include/node_config_*.h` for ttpa pins configuration.
See `ttpa/include/node_config.h` and Makefiles for macro definition.

## COMPILATION ##

Run Makefile in any subfolder. Use avr-gcc compiler with version not later then 4.3.5.