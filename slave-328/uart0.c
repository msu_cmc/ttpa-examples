#include <avr/io.h>
#include <util/parity.h>
#include <stdio.h>

#include "uart0.h"

static int uart0_putchar(char c, FILE *stream);
static int uart0_getchar(FILE *stream);

FILE uart0 = FDEV_SETUP_STREAM(uart0_putchar, uart0_getchar, _FDEV_SETUP_RW);
int8_t uart0_rx_overun = 0;

static int uart0_putchar(char c, FILE *stream) 
{
//  if (c == '\n') uart0_putchar('\r', stream);
  loop_until_bit_is_set(UCSR0A, UDRE0);
  if (parity_even_bit(c)) UCSR0B |= _BV(TXB80);
  else UCSR0B &= ~_BV(TXB80);
  UDR0 = c;
  return 0;
}

static int uart0_getchar(FILE *stream) 
{
	int c;
	loop_until_bit_is_set(UCSR0A, RXC0);
	// detect frame error, parity error or buffer overrun error
	uart0_rx_overun |= UCSR0A & _BV(DOR0);
	if (UCSR0A & (_BV(FE0)|_BV(UPE0)|_BV(DOR0))) {
		c = UDR0;
		return _FDEV_ERR;
	} 
	else return UDR0;
}

void uart0_init(void) 
{
	// Set baurdrate
/*#define BAUD 57600
#include <util/setbaud.h>
	UBRR0L = UBRRL_VALUE;
	UBRR0H = UBRRH_VALUE;*/
	// Set mode 
	UBRR0 = 16;
	UCSR0A = 0;
/*#ifdef USE_2X
	UCSR0A |= _BV(U2X0);
#endif*/
	// TX enabled, RX disable, interrupts disabled, 9 data bits, 1 stop bits, even parity
	UCSR0B = _BV(TXEN0) | _BV(UCSZ02) ;
	UCSR0C =  _BV(UCSZ00) | _BV(UCSZ01) ;
}
