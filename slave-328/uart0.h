/** @file uart0.h
 * @brief UART link interface.
 */
#ifndef UART0_H
#include <stdio.h>

extern FILE uart0;
extern int8_t uart0_rx_overun;

extern void uart0_init(void); 

#endif
