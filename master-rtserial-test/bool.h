/** 
@file bool.h
@brief Boolean type defines.
*/
#ifndef BOOL_H
#define BOOL_H

#define BOOL uint8_t
#define TRUE ((uint8_t) 1)
#define FALSE ((uint8_t) 0)

#endif

