#include <stdio.h>
#include "ifs.h"
#include "ifs_types.h"
#include "ifs_rodl.h"
#include "ttpa_task.h"

#include "appl.h"

// Receive data from slaves
IFS_RODLFILE(0x00, 3, ifs_int_eep)
{
	// Recv data from slaves and place it in RTSEND file: receive 2 records from each slave (speedp=8)
	IFS_RE_RECV_SPDUP(3, IFS_ADDR_I(RTSEND_FN, 1, 0), NSLAVE-1, 1),
	// Execute rtserial_task: send data : align = 0x2
	IFS_RE_EXEC(11, IFS_ADDR_I(RTSERIAL_FN, 1, 0x2)),
	IFS_RE_EOR(12),
};

// Send data to slaves
IFS_RODLFILE(0x02, 5, ifs_int_eep)
{
	// Execute rtserial_task:  receive data: align == 0x1 
	IFS_RE_EXEC(1, IFS_ADDR_I(RTSERIAL_FN, 1, 0x1)),
	// Send data to slaves from RTRECV file: send 2 record to each slave (speedup = 8)
	IFS_RE_SEND_SPDUP(3, IFS_ADDR_I(RTRECV_FN, 1, 0), NSLAVE-1, 1),
	IFS_RE_EOR(12),
};
