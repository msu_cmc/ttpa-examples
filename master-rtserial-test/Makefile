#   Project:     TTP/A Protocol for Atmel AVR
#   
#   File:        $RCSfile: Makefile.linux,v $ 
#   Description: common definitions for all Makefiles (Linux)
#   Revision     $Revision: 22 $
#   Date:        $Date: 2005-12-09 11:20:28 +0100 (Fre, 09 Dez 2005) $
#   Author:      Christian Troedhandl
#   Adopted by:  Alexander Koessler
#
#   TU Vienna, Real Time Group


PRJNAME = master

# Node Number
TARGET = 0

# Processor
MCU = atmega2560

# Select programming Interface
INTERFACE  = /dev/avr-programmer

# Define Debug Port
DEBUGPORT  = 6000

# all object files for the user's code
OBJS= rodl.io rose.io appl.io rtsend.o rtrecv.o 

#============================================================
# usually there is no need to change anything below this line
# but be carefull: the trojans placed the horse in the city
# because of their lack of knowledge ...
#============================================================

PREFIX = /usr/bin
AS     = $(PREFIX)/avr-as
LD     = $(PREFIX)/avr-ld 
CC     = $(PREFIX)/avr-gcc 
OBJCOPY= $(PREFIX)/avr-objcopy

PROGR      = avrdude
PROGR_FLAGS= -p $(MCU) -P $(INTERFACE) -c avrispv2 


TTPAROOT = ../ttpa
SCRIPTROOT = $(TTPAROOT)/scripts
ELF_FILE   = $(PRJNAME).elf

IFSCONV = $(SCRIPTROOT)/conv_byteorder.pl

ADD_NODECONF = -DBAUDRATE=64000 -DUSE_RODLCACHE -DTIMER_16BIT -DPROFILE -DPROFILE_TIMER_OVF -DTIMER_FIXEDSLOT -DXHW_UART_SYNCRONOUS 
NODECONF = -DNODECONF_MEGA2560 -DF_CPU=16000000 -DLN_NODE=0xfe -DDEBUG_PUTCHAR -DDEBUG_BAUDRATE=115200 -DHW_UART -DUSE_SCHEDULE -DMASTER $(ADD_NODECONF)         

# add Include dirs here
IDIR   = -I./ -I$(TTPAROOT)/include/
LDDIR  = $(TTPAROOT)/ldscripts/

# select output format for *.hex and *.eep files
# srec: Motorola srec; ihex: Intel ihex
#FORMAT = srec
FORMAT = ihex
CFLAGS = -std=gnu99 -Os $(IDIR) -mmcu=$(MCU) $(NODECONF)
ASFLAGS=  $(IDIR) -mmcu=$(MCU) -Wa,-mmcu=$(MCU) $(NODECONF)
LDFLAGS= -Xlinker -T -Xlinker $(LDDIR)$(MCU).x -mmcu=$(MCU)


# all object files for the protocol code
MONOBJS= mon_statem.o mon_uart.o ms_request.o
TTPAOBJS= debug.o mutex.o bus_hwuart.o ifs.o ifs_bitvec.o ifs_rodl.o ifs_weaks.o \
ifs_filetab.o ifs_init.o ifs_exechrec.o ms.io ms_master.io ttpa.o ttpa_timer.o ttpa_fb.o \
ttpa_lifesign.o ttpa_master.io ttpa_sysfiles.io schedule.o main.o
HEADERS= 
vpath %.h ./:$(TTPAROOT)/include/
vpath %.c ./:$(TTPAROOT)/ttpa/
vpath %.S ./:$(TTPAROOT)/ttpa/

all: $(TTPAOBJS) $(MONOBJS) $(OBJS) $(PRJNAME).elf $(PRJNAME).eep $(PRJNAME).hex

%.s: %.c
	$(CC) $(CFLAGS) -S $< -o $@

%.is: %.s
	$(IFSCONV) <$< >$@

%.io: %.is
	$(CC) $(ASFLAGS) -x assembler -c $< -o $@

%.elf:	$(TTPAOBJS) $(MONOBJS) $(OBJS)
	$(CC) $(TTPAOBJS) $(MONOBJS) $(OBJS) $(IFSOBJ) $(LIB) $(LDFLAGS) -o $@

%.bin:	$(PRJNAME).elf
	$(OBJCOPY) -O binary $< $@

%.hex:  %.elf
	$(OBJCOPY) -j .text -j .data -j .ifs_int_flash -j .ifs_ext_flash \
	-O $(FORMAT) $< $@

%.eep:  %.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom-0x810000 \
        --set-section-flags=.eeprom="alloc,load" \
        -j .ifs_eep --change-section-lma .ifs_eep-0x810000 \
        --set-section-flags=.ifs_eep="alloc,load" \
        -j .ifs_int_eep --change-section-lma .ifs_int_eep-0x810000 \
        --set-section-flags=.ifs_int_eep="alloc,load" \
        -j .ifs_ext_eep --change-section-lma .ifs_ext_eep-0x810000 \
        --set-section-flags=.ifs_ext_eep="alloc,load" -O $(FORMAT) $< $@

install: all #installs program to selected target
	$(PROGR) $(PROGR_FLAGS) -U flash:w:$(PRJNAME).hex
	$(PROGR) $(PROGR_FLAGS) -U eeprom:w:$(PRJNAME).eep

eeprom:
	$(PROGR) $(PROGR_FLAGS) -U eeprom:w:$(PRJNAME).eep

clean:
	$(RM) *.s *.is *.o *.io *.eep *.hex *.elf



