#include <avr/io.h>
#include <util/atomic.h>

#include <stdlib.h>
#include <string.h>

#include "config.h"

#include "rtrecv.h"


#if ! defined(RTRECV_FRAME_LENGTH)
// dynamic memmory allocation
	static char * buffer1;
	static char * buffer2;
	static size_t frame_length;

	static char * accum;
	static char * recv_buffer;

	size_t rtrecv_frame_length(void) {
		return frame_length;
	}
#else 
//static memory allocation
	static char buffer1[RTRECV_FRAME_LENGTH]
	static char buffer2[RTRECV_FRAME_LENGTH]
#	define frame_length RTRECV_FRAME_LENGTH

	//TODO volatile?
	static char * accum = buffer1;
	static char * recv_buffer = buffer2;
#endif

static volatile rtrecv_mode_t mode = RTRECV_STOP;
static volatile BOOL accum_updated, accum_need_update, accum_lock;

//state machine state constants
enum RTRecvStateInternal {
	STOPPED				= 0x00,
	RECV_SYNC_STARTBYTE	= 0x11,
	RECV_SYNC_DATA		= 0x12,
	RECV_SYNC_TAILBYTE	= 0x13,
	RECV_STARTBYTE		= 0x01,
	RECV_DATA			= 0x02,
	RECV_TAILBYTE		= 0x03,
	SYNC_MASK			= 0x10,
};

//state machine variables
static rtrecv_state_t state = STOPPED; 
static uint8_t uart_error_flags;
static size_t cursor;

rtrecv_state_t rtrecv_get_state(void)
{
	if (state == STOPPED) return RTRECV_STOPPED;
	else if (state & SYNC_MASK) return RTRECV_UNSYNC;
	else return RTRECV_ACTIVE;
}

void rtrecv_set_mode(rtrecv_mode_t _mode)
{
	if (_mode == RTRECV_RECV_WAIT_ACCUM || _mode == RTRECV_RECV_CONT || \
			_mode == RTRECV_RECV_STOP || _mode == RTRECV_STOP) 
	{
		if (frame_length == 0) mode = RTRECV_STOP;
		else mode = _mode;
#if defined(RTRECV_ISR)
		if (mode == RTRECV_RECV_CONT || mode == RTRECV_RECV_WAIT_ACCUM) {
			// Enable RXC interupt.
			if (state == STOPPED) RT_UCSRB |= _BV(RT_RXCIE);
		}
#endif
	}
}

BOOL rtrecv_is_accum_updated(void) {
	return accum_updated;
}

void * rtrecv_get_accum_lock(void) {
	accum_lock = TRUE;
	return accum;
}

void rtrecv_accum_release(void) {
	accum_updated = FALSE;
	accum_lock = FALSE;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (accum_need_update) {
			register char * tmp;
			tmp = accum;
			accum = recv_buffer;
			recv_buffer = tmp;
			accum_updated = TRUE;
			accum_need_update = FALSE;
		}
	}
}

#if defined(RTRECV_ISR) 

ISR(RT_RX_SIG) 
{
	char uart_byte;

	// Get UART data and flags.
	uart_error_flags |= RT_UCSRA & (_BV(RT_FE) | _BV(RT_PE) | _BV(RT_DOR));
	uart_byte = RT_UDR;

	// Disable RXC interrupt until byte is processed
	RT_UCSRB &= ~_BV(RT_RXCIE);
	sei();

	if (mode == RTRECV_STOP) { 
		state = STOPPED; 
		// Do not enable interrupt again!
		return;
	}

#else

void rtrecv_poll(void) 
{
	char uart_byte;

	if (mode == RTRECV_STOP) { 
		state = STOPPED; 
		return;
	}

	// We are going to receive data. Check if byte receive complete
	if ( !(RT_UCSRA & _BV(RT_RXC)) ) {
		// TODO in poll mode update accum if necessary
		return;
	}
	// Get UART data and flags.
	uart_error_flags |= RT_UCSRA & (_BV(RT_FE) | _BV(RT_PE) | _BV(RT_DOR));
	uart_byte = RT_UDR;

#endif

	if ( uart_error_flags & RT_DOR ) {
		// Data overrun. Attempt to sync again.
		state = RECV_SYNC_STARTBYTE;
	}
				
	switch (state) {
		// NORMAL OPERATION
		case RECV_STARTBYTE:
			if (uart_byte != STARTBYTE) {
				// Incorrect frame. Attempt to sync again.
				state = RECV_SYNC_STARTBYTE;
				break;
			}
			cursor = 0;
			state = RECV_DATA;
			accum_need_update = FALSE; // we begin to fill recv_buffer
			break;
		case RECV_DATA:
			recv_buffer[cursor] = uart_byte;
			cursor++;
			if (cursor == frame_length) {
				state = RECV_TAILBYTE;
			}
			break;
		case RECV_TAILBYTE:
			if (uart_byte != TAILBYTE) {
				// Incorrect frame. Attempt to sync again.
				state = RECV_SYNC_STARTBYTE;
				break;
			}
			// if frame is corrupted, skip it
			if (!uart_error_flags) {
				// Attempt to update accumulator content.
				if (mode == RTRECV_RECV_WAIT_ACCUM) {
					ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
						if (!accum_lock && !accum_updated) {
							register char * tmp;
							tmp = accum;
							accum = recv_buffer;
							recv_buffer = tmp;
							accum_updated = TRUE;
						}
						else {
							accum_need_update = TRUE;
						}
					}
				}
				else {
					ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
						if (!accum_lock) {
							register char * tmp;
							tmp = accum;
							accum = recv_buffer;
							recv_buffer = tmp;
							accum_updated = TRUE;
						}
						else {
							accum_need_update = TRUE;
						}
					}
				}
			}
			// switch state according to operational mode
			switch (mode) {
				case RTRECV_RECV_CONT:
				case RTRECV_RECV_WAIT_ACCUM:
					state = RECV_STARTBYTE;
					break;
				case RTRECV_RECV_STOP:
				case RTRECV_STOP: 
					state = STOPPED;
					break;
				default:
					// errorous state
					state = STOPPED;
					break;
			}
			break;
		// SYNCRONIZATION
		case RECV_SYNC_STARTBYTE:
			if (uart_byte != STARTBYTE) break;
			cursor = 0;
			uart_error_flags = 0;
			state = RECV_SYNC_DATA;
			accum_need_update = FALSE;
			break;
		case RECV_SYNC_DATA:
			recv_buffer[cursor] = uart_byte;
			cursor++;
			if (cursor == frame_length) {
				state = RECV_SYNC_TAILBYTE;
			}
			break;
		case RECV_SYNC_TAILBYTE:
			{
				if (uart_byte != TAILBYTE) {
					char * ptr;
					// incorrect frame
					// find new startbyte
					if (uart_byte == STARTBYTE) {
						// new STARTBYTE candidate just received
						cursor = 0;
						uart_error_flags = 0;
						state = RECV_SYNC_DATA;
						break;
					}
					// try to find STARTBYTE in buffer
					ptr = memchr(recv_buffer, STARTBYTE, cursor);
					if (ptr != 0) {
						// next STARTBYTE is in the buffer
						ptr++;
						cursor = (recv_buffer + cursor) - ptr; // calculate the length of data recived after STARTBYTE
						memmove(recv_buffer, ptr, cursor);
						// add just received byte
						recv_buffer[cursor] = uart_byte;
						// if buffer is not full, continue receving data bytes
						if (cursor != frame_length) {
							state = RECV_SYNC_DATA;
						}
					}
					else {
						// STARTBYTE has not received yet
						state = RECV_SYNC_STARTBYTE;
					}
					break;
				}
			}
			// correct frame has been received: switch to normal mode
			state = RECV_STARTBYTE;
			break;
		case STOPPED:
			if (mode == RTRECV_RECV_CONT || mode == RTRECV_RECV_WAIT_ACCUM) {
				state = RECV_SYNC_STARTBYTE;
			}
			else {
				// Do not enable RXC interrupt	
				return;
			}
			break;
		default:
			// errorous state
			state = STOPPED;
			// Do not enable RXC interrupt	
			return;
	}
#if defined(RTRECV_ISR)
	// Enable RXC interupt again: received byte processing has been finished.
	RT_UCSRB |= _BV(RT_RXCIE);
#endif
}

BOOL rtrecv_init(size_t length)
{
#if defined(RTRECV_ISR) 
	//TODO Check if ISR is running
	mode = RTRECV_STOP;
	state = STOPPED;
#else
	if (state != STOPPED) return FALSE;
#endif
	// Set baurdrate
	RT_UBRR = RT_UBRR_VALUE;

	// RX enbled, interrupts disabled, 8 data bits, 1 stop bits, even parity
	RT_UCSRB |= _BV(RT_RXEN);
	RT_UCSRB &= ~(_BV(RT_UCSZ2) | _BV(RT_RXCIE));
	RT_UCSRC |=  _BV(RT_UCSZ0) | _BV(RT_UCSZ1) | _BV(RT_PM1);
	RT_UCSRC &= ~(_BV(RT_USBS) | _BV(RT_PM0));

#if !defined(RTRECV_FRAME_LENGTH) 
	free(buffer1);
	free(buffer2);
	frame_length = 0;
	buffer1 = malloc(length);
	if (buffer1 == 0) return FALSE;
	buffer2 = malloc(length);
	if (buffer2 == 0) return FALSE;
	accum = buffer2;
	recv_buffer = buffer1;
	frame_length = length;
#endif
	return TRUE;
}

BOOL rtrecv_deinit(void) 
{
#if defined(RTRECV_ISR) 
	//TODO Check if ISR is running
	mode = RTRECV_STOP;
	state = STOPPED;
#else
	if (state != STOPPED) return FALSE;
#endif

	RT_UCSRB &= ~(_BV(RT_RXEN) | _BV(RT_RXCIE));
#if !defined(RTRECV_FRAME_LENGTH) 
	free(buffer1);
	free(buffer2);
	frame_length = 0;
	buffer1 = 0;
	buffer2 = 0;
	accum = 0;
	recv_buffer = 0;
#endif
	return TRUE;
}
