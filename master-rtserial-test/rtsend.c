#include <avr/io.h>
#include <util/atomic.h>

#include <stdlib.h>
#include <string.h>

#include "config.h"

#include "rtsend.h"

#if !defined(RTSEND_FRAME_LENGTH)
	static char * buffer1;
	static char * buffer2;
	static size_t frame_length;

//TODO: volatile?
	static char * accum;
	static char * send_buffer;

	size_t rtsend_frame_length(void) {
		return frame_length;
	}
#else 
	static char buffer1[RTSEND_FRAME_LENGTH]
	static char buffer2[RTSEND_FRAME_LENGTH]
#	define frame_length RTSEND_FRAME_LENGTH

//TODO: volatile?
	static char * accum = buffer1;
	static char * send_buffer = buffer2;
#endif


static volatile rtsend_mode_t mode = RTSEND_STOP;
static volatile BOOL accum_updated;

//state machine state constants
enum RTSendStateInternal {
	STOPPED				= 0x00,
	PREPARE				= 0x01,
	SEND_STARTBYTE		= 0x02,
	SEND_DATA			= 0x03,
	SEND_TAILBYTE		= 0x04,
};

//state machine variables
static rtsend_state_t state = STOPPED; 
static size_t cursor;

rtsend_state_t rtsend_get_state(void)
{
	if (state == PREPARE) return RTSEND_WAITING;
	else if (state != STOPPED) return RTSEND_ACTIVE;
	else return RTSEND_STOPPED;
}

void rtsend_set_mode(rtsend_mode_t _mode)
{
	if (_mode == RTSEND_SEND_WAIT_ACCUM || _mode == RTSEND_SEND_CONT || \
			_mode == RTSEND_SEND_STOP || _mode == RTSEND_STOP) 
	{
		if (frame_length == 0) mode = RTSEND_STOP;
		else mode = _mode;
#if defined(RTSEND_ISR)
		switch (mode) {
			case RTSEND_SEND_CONT:
				if (state == PREPARE || state == STOPPED) {
					// Enable UDRE interupt.
					state = SEND_STARTBYTE;
					RT_UCSRB |= _BV(RT_UDRIE);
				}
				break;
			case RTSEND_SEND_WAIT_ACCUM:
				if (state == STOPPED) {
					// Enable UDRE interupt.
					state = SEND_STARTBYTE;
					RT_UCSRB |= _BV(RT_UDRIE);
				}
				break;
		}
#endif
	}
}

BOOL rtsend_is_accum_yanked(void) {
	return !accum_updated;
}

void * rtsend_get_accum_lock(void) {
	accum_updated = FALSE;
	return accum;
}

void rtsend_accum_release(void) {
#if defined(RTSEND_ISR)
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		accum_updated = TRUE;
		if (state == PREPARE) {
			state = SEND_STARTBYTE;
			RT_UCSRB |= _BV(RT_UDRIE);
		}
	}
#else 
	accum_updated = TRUE;
#endif
}

#if defined(RTSEND_ISR)

ISR(RT_TX_SIG) 
{
	// Disable UDRE interrupt until byte is processed
	RT_UCSRB &= ~_BV(RT_UDRIE);
	sei();

	if (mode == RTSEND_STOP) { 
		state = STOPPED; 
		return;
	}

	switch (state) {
		case SEND_DATA:
			RT_UDR = send_buffer[cursor];
			cursor++;
			if (cursor == frame_length) {
				state = SEND_TAILBYTE;
			}
			break;
		case SEND_STARTBYTE:
			// check accum state, select data to sent
			ATOMIC_BLOCK(ATOMIC_FORCEON) {
				if (accum_updated) {
					register char * tmp;
					tmp = accum;
					accum = send_buffer;
					send_buffer = tmp;
					accum_updated = FALSE;
				}
				else if (mode == RTSEND_SEND_WAIT_ACCUM) {
					// Wait until accum is updated: do not enable UDRE interrupt.
					state = PREPARE;
					return;
				}
			}
			RT_UDR = STARTBYTE;
			state = SEND_DATA;
			cursor = 0;
			break;
		case SEND_TAILBYTE:
			RT_UDR = TAILBYTE;
			if (mode == RTSEND_SEND_STOP) {
				state = STOPPED;
				return;
			}
			else state = SEND_STARTBYTE;
			break;
	}
	// Enable URDE interrupt again
	RT_UCSRB |= _BV(RT_UDRIE);
}

#else /* RTSEND_ISR */

void rtsend_poll(void) 
{
	if (mode == RTSEND_STOP) { 
		state = STOPPED; 
		return;
	}

	if (state == PREPARE) {
		// check accum state, select data to sent
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			if (accum_updated) {
				register char * tmp;
				tmp = accum;
				accum = send_buffer;
				send_buffer = tmp;
				accum_updated = FALSE;
				state = SEND_STARTBYTE;
			}
		}

		switch (mode) {
			case RTSEND_SEND_CONT:
			case RTSEND_SEND_STOP:
				// start transmittion even if send buffer is not updated
				state = SEND_STARTBYTE;
				break;
			case RTSEND_SEND_WAIT_ACCUM:
				// exit if bufer is not updated
				if (state == PREPARE) return;
				break;
			case RTSEND_STOP: 
				state = STOPPED;
				return;
			default:
				// errorous state
				state = STOPPED;
				return;
		}
	}

	// We are going to trnsmit date. Check if transmitter is ready.
	if ( !(RT_UCSRA & _BV(RT_UDRE)) ) {
		// Transmitter is not ready, so continue polling.
		return;
	}
				
	switch (state) {
		case SEND_STARTBYTE:
			RT_UDR = STARTBYTE;
			state = SEND_DATA;
			cursor = 0;
			break;
		case SEND_DATA:
			RT_UDR = send_buffer[cursor];
			cursor++;
			if (cursor == frame_length) {
				state = SEND_TAILBYTE;
			}
			break;
		case SEND_TAILBYTE:
			RT_UDR = TAILBYTE;
			if (mode == RTSEND_SEND_STOP) state = STOPPED;
			else state = PREPARE;
			break;
		//STOPPED
		case STOPPED:
			if (mode == RTSEND_SEND_CONT || mode == RTSEND_SEND_WAIT_ACCUM) {
				state = PREPARE;
			}
			break;
	}
}
#endif /* RTSEND_ISR */    


BOOL rtsend_init(size_t length)
{
#if defined(RTSEND_ISR) 
	//TODO Check if ISR is running
	mode = RTSEND_STOP;
	state = STOPPED;
#else
	if (state != STOPPED) return FALSE;
#endif

	// Set baurdrate
	RT_UBRR = RT_UBRR_VALUE;

	// TX enbled, interrupts disabled, 8 data bits, 1 stop bits, even parity
	RT_UCSRB |= _BV(RT_TXEN);
	RT_UCSRB &= ~(_BV(RT_UCSZ2) | _BV(RT_TXCIE) | _BV(RT_UDRIE));
	RT_UCSRC |=  _BV(RT_UCSZ0) | _BV(RT_UCSZ1) | _BV(RT_PM1);
	RT_UCSRC &= ~(_BV(RT_USBS) | _BV(RT_PM0));

#if !defined(RTSEND_FRAME_LENGTH) 
	free(buffer1);
	free(buffer2);
	frame_length = 0;
	buffer1 = malloc(length);
	buffer2 = malloc(length);
	if (buffer1 == 0 || buffer2 == 0) return FALSE;
	accum = buffer2;
	send_buffer = buffer1;
	frame_length = length;
#endif
	return TRUE;
}

BOOL rtsend_deinit(void) 
{
#if defined(RTSEND_ISR) 
	//TODO Check if ISR is running
	mode = RTSEND_STOP;
	state = STOPPED;
#else
	if (state != STOPPED) return FALSE;
#endif

	RT_UCSRB &= ~(_BV(RT_TXEN) | _BV(RT_UDRIE));
#if !defined(RTSEND_FRAME_LENGTH) 
	free(buffer1);
	free(buffer2);
	frame_length = 0;
	buffer1 = 0;
	buffer2 = 0;
	accum = 0;
	send_buffer = 0;
#endif
	return TRUE;
}
