/** @file config.h
 * @brief Program wide defines.
 */
#ifndef  CONFIG_H
#define  CONFIG_H

#include "node_config.h"

#define UART0_BAUDRATE 57600

#define RT_BAUDRATE 115200
#define RT_ISR
#if defined(RT_ISR) 
#	define RTSEND_ISR
#	define RTRECV_ISR
#endif

#endif  /*CONFIG_H*/
